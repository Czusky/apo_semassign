#include "cstdint"
#include "mbed.h"

#ifndef DFPLAYER_H
#define DFPLAYER_H
extern UnbufferedSerial DFP;

#define STRT_BT 0x7E
#define END_BT 0xEF
#define FB 0x01
#define NO_FB 0x00
#define VRSN 0xFF
#define LEN_BT 0x06

#define CMD_SELECT 0x0F
#define CMD_PLAY 0x0D
#define CMD_STOP 0x16

/**
 * Play selected tranck from selected folder - CAUTION - no checks done, files must exist on SD-Card for proper
 *functionality
 *
 * @param file uint8_t, specifies file
 * @param track uint8_t, specifies track
 **/
void dfp_play(uint8_t file, uint8_t track);

/**
 * @brief Stops all audio decoding and playback
 **/
void dfp_stop(void);

#endif
