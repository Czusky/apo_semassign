#include "Shared.h"
#include <mbed.h>

/**
 * API definition: REST API, based on HTTP requests:
 * GET: expects JSON object {"time":\timestamp\} where \timestamp\ is (max) 10-digit integer
 * representing UNIX timestamp in seconds
 * POST: sends JSON object {"bedtime":\timestamp\,"waketime":\timestamp\,"start":\bool\} where \timestamp\
 * is (max) 10-digit integer representing UNIX timestamp in seconds, \bool\ is 1 or 0 depending if alarm is
 * being activated or cancelled respectively
 **/

/**
 * @brief Get actual timestamp
 **/
int sync_time(void);

/**
 * Send data to server
 * @param bedTime actual timestamp (time_t)
 * @param wakeTime timestamp of desired wakeup time (time_t)
 * @param start indicate if cancelling or strting wake (bool)
 **/
void post_request(time_t bedTime, time_t wakeTime, bool start);

// void delete_request(time_t bedTime, time_t wakeTime);

/**
 * @brief Connect to WiFi specified in shared data
 **/
bool connect_wifi(void);

/**
 * @brief Reset WiFi module
 **/
bool reset_module(void);