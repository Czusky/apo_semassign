#include "Shared.h"
#include "ESP12-F.h"
#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306_I2c.h"

#ifndef SCREENS_H
#define SCREENS_H

extern I2C i2cBus;
extern Adafruit_SSD1306_I2c display;

#endif

#define HOME 0
#define STTNGS 1
#define GRAPH 2
#define PICK 3
#define SET_TIME 4
#define RING 5

#define AL_1 11
#define AL_2 12
#define AL_3 13
#define AL_4 14
#define AL_5 15
#define AL_6 16

#define CNCL 21
#define SYNC 22

/**
 * @brief Class for handling elements (buttons) on screen
 */
class Element
{
 public:
   bool selected = false;
   bool set = false;

   Element() {}

   /**
    * @brief Prints element on screen
    **/
   void show(void);

   /**
    * @brief Constructs element with specified parameters
    * @param r row (short)
    * @param c collumn (short)
    * @param txt text of element (string)
    * @param nxt identifier of action / following screen (short)
    * @param size size of text (short)
    **/
   void cnstrct(short r, short c, string txt, short nxt, short size);

   /**
    * @brief Reacts to short button press
    **/
   void action(void);

 private:
   short sz;
   short x;
   short y;
   short w;
   short h;
   string text;
   short next;
};

/**
 * @brief Class for handling screens
 */
class Screen
{
 public:
   Element elms[6];
   const int prev;

   /**
    * @brief Screen class constructor
    * @param tp type identifier (int)
    * @param prv identifier of previous screen (int)
    **/
   Screen(int tp, int prv);

   /**
    * @brief Prints screen to display
    **/
   void show(void);

   /**
    * @brief Reacts to stick change, action based on active screen
    **/
   void move(short dir);
   short slctdIndx; ///< index of currently selected element on screen

 private:
   const int type;
   void (Screen::*ptr)() = nullptr;
   int nBtns;
   short rws;
   short clls;
   short r;
   short c;

   /**
    * @brief Printing funciton for home screen
    **/
   void home(void);

   /**
    * @brief Printing funciton for settings screen
    **/
   void sttngs(void);

   /**
    * @brief Printing funciton for graph screen
    **/
   void graph(void);

   /**
    * @brief Printing funciton for wake time selection screen
    **/
   void pick(void);

   /**
    * @brief Printing funciton for time setting screen
    **/
   void set_time(void);

   /**
    * @brief Printing funciton for ringing screen
    **/
   void ring(void);
};
