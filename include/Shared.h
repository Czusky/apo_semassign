#include "mbed.h"

#ifndef SHARED_H
#define SHARED_H

/** 
 * @brief Struct for holding shared data
 */
typedef struct shrd {
   short activeScrn;  ///< identifier of active screen
   bool alrmStatus;   ///< indicate of alarm is set
   time_t wakeTime;   ///< time to which alarm is set
   time_t bedTime;    ///< time at which alaram was set
   bool ringing;      ///< indicate active ringing
   char ssid[50];     ///< ssid of wifi to be connected to
   char password[50]; ///< password for wifi
   bool wifiOk;       ///< wifi connected
   short bpm;         ///< current heartrate
   bool newBpm;       ///< indicate new heartrate reading is ready
} data;

extern data shared;

#endif