# Project description
This project is quasi *smart alarm clock*, based on sleep cycle optimisation: Offers estimated optimal times for waking up based on current time. The alarm will be triggered within specified interval, the exact timing is dependent on data from heartrate sensor. In case treshold condition won't be met before the end of the interval, alarm will sound regardless.

# Instructions
## Hardware
This project is very hardware specific, may require modification. Tested combination is on *ST STM32 Nucleo F446RE* as the main MCU, with *ESP12-F WiFi module* (compactibility depends on specific firmware, Serial interface on pins PC_10, PC_11), *DFPlayer mini* (with microSD card with tracks in specified format, Serial interface on pins D2, D8), monochromatic *SH1106 based OLED display 124x68* using I2C interface (on pins D14, D15), analog joystick (with 10k potentiometers and integrated button, axies on pins A0, A1, button on A2) and an alalog heartrate sensor (on pin A5).

## Compilation
Project is built on Mbed framework, via PlatformIO, which is required for compilaton. The purpose of that is to make porting between platforms easier. For this reason, there are two options how to go about getting ready for compiling this project. Both rely on PlatformIO, use whichever option you prefer. *Option 1* is more straightforward, while *Option 2* should offer a more flexibility for porting.

### Option 1
1. Clone the *All-project-files* branch. (Alternatively use whole contents of zip archive)
2. Open PlatformIO
    1. *PIO Home*
    2. *Projects*
    3. *Add existing*
3. Select the cloned folder.

**Note** which pins are used where is specified in the header file corresponding to the periphery.

### Option 2
1. Clone the *master* branch. (Alternatively use only contents of **src** and **include** from the zip archive)
2. Open PlatformIO
3. Create an empty project
    1. *PIO Home*
    2. *Projects*
    3. *Create new*
4. Select your board and Mbed framework
5. After project is initialised, a library needs to be installed: **Adafruit_GFX_mbed_oled** library by DaveTcc
    1. *PIO Home*
    2. *Libraries*
    3. put *Adafruit_GFX_mbed_oled* into the searchbar
    4. click on corresponding entry
    5. *Add to project*
    6. select the project you created
6. copy the source (.cpp) files into **src** folder and the header files (.h) into **include** folder.

### Completing compilaton
Before compiling, for full functionality, set your WiFi credentials inside the *shared data* structure inside of *main.cpp* and in the *ESP12-F.h* fill in your server parameters. Inside the *main.cpp* are few other parameters available for change. 

Make sure, that you're in the correct PIO Project enviroment (if you have only this project, you shouldn't need to) and use the PlatformIO *Build* option. If you have your board connected and have granted all the permissions for PlatformIO, the option *Upload* can be used for compiling and directly uploading the project to your MCU.

**Note** which pins used where is specified in the header file corresponding to the periphery.

## Usage
Navigation in the GUI is done by the joystick, allows selecting only orthogonally neighbouring selectable cells. By short press, you activate the action (if defined) of selected cell. Using a long press you can return to previous page, with the exception of homepage.

**Note** that the display will turn off after specified time without any joystick action (with the exeption of the *Graf* page). To wake up simply move the joystick. Also any communication over WiFi is blocking, therefore will take some time and seemingly "freeze" the app, maximally for few seconds (depends if WiFi has lost connection and other factors). Since upon startup the module attempts initialize WiFi connection it may take a while for anything to show up on screen.

### GUI pages
**Note** the GUI is exclusively in Czech. For the purposes of documentation, English will be used with corresponding \:*translation*:\ noted, which is displayed on screen
#### Alarm
\:*Budik*:\

On the *Alarm* page, you can pick desired wakeup time by selecting it and then pressing the button (short press)
#### Settings
\:*Nastaveni*:\

Here are few settings available:

* **Set time**
\:*Nastaveni casu*:\
    * lets you set time manually by joystick movements, as described on screen, confirm by short press (not strictly nescessary)

* **Synchronise time**
\:*Synchronizace casu*:\
    * requests current time from your server (using REST API and HTTP GET request)

* **Cancel alarm**
\:*Vypnout budik*:\
    * cancels alarm (only if active, then also sends that information to the server, otherwise just returns to homepage)
* **Graph**
\:*Graf*:\
    *On the *Graph* page, a graphical interpretation of heartrate monitor data is being graphed out. It displays the heartrate in BPM. Its speed can be set in *main.cpp* file

### Alarm usage
The process of setting or cancelling the alarm is described in the **GUI pages** section. Once alarm is bieng sounded, a screen saying *Get up* { *Vstavat* } is displayed. Alarm can be stopped by long pressing the button. There is no option for manually postponing or repeating alarms.

#### SD card formatting and filesystem
The DFPlayer mini accepes microSD cards with filsesystems FAT16 and FAT32. This program expects that it contains at least one folder (sequentially named *01*, *02*,...,*99*) containing at least one track (sequentially named *001.mp3*/001.wav, *002.mp3*/*002.wav*,...,*255.mp3*/*255.wav*). The track came can preserve its original name after the number: e.g. track named "*El Dorado.mp3*" can be put into the file as "*001El Dorado.mp3*".

The ringtone track is specified in *main.cpp* by specifying number of folder and number of the track, both as hexadecimal integers.

## API Definition
Communication is based on REST API using HTTP GET and POST requests.

**Note** This functionality is highly server dependent. With tested firmware were issues with trying to connect to Wedos and Cloudlfare hosted servers (requests didn't get through)

### GET
Expects in return JSON object in following form
```
{
    "time":-timestamp-
}
```
where -timestamp- is an integer of max. 10 digits, representing the Unix timestamp in seconds.

### POST
Expects no answer, sends JSON object in following form
```
{
    "bedtime":-timestamp-,
    "waketime":-timestamp-,
    "start":-bool-
}
```
where -timestamp- is an integer of max. 10 digits, representing the Unix timestamp in seconds and -bool- indicates, whether this alarm is being activated or cancelled, represented by 1 or 0 respectively.
