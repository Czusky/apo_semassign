#include "DFPlayer.h"

UnbufferedSerial DFP(D8, D2, 9600);

/**
 * @brief Local function for computing checksum and sending command to DFPlayer mini
 *
 **/
void send(uint8_t len, uint8_t cmd, uint8_t fdbck, uint8_t paraMsb, uint8_t paraLsb)
{
   uint16_t chck = (0xFFFF - (VRSN + len + cmd + fdbck + paraMsb + paraLsb) + 1);
   uint8_t chckMsb = (0xFF00 & chck) >> 8;
   uint8_t chckLsb = 0x00FF & chck;

   uint8_t buf[10] = {STRT_BT, VRSN, len, cmd, fdbck, paraMsb, paraLsb, chckMsb, chckLsb, END_BT};
   DFP.write(&buf, 10);
}

void dfp_play(uint8_t file, uint8_t track)
{
    send(LEN_BT, CMD_SELECT, NO_FB, file, track);
}

void dfp_stop(void)
{
    send(LEN_BT, CMD_STOP, NO_FB, 0x00, 0x00);
}