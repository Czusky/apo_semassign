#include "ESP12-F.h"

BufferedSerial esp(PC_10, PC_11, 9600);
FILE *espFile = fdopen(&esp, "r+");

bool echo = true;
bool cnnctd = false;

char protocol[10] = "http://";
char url[50] = "example.com";
char postAddr[50] = "/postexample";
char getAddr[50] = "/getexample";

/**
 * Local function, wait for and read data from ESP
 * @param origin caller ID for debugging purposes
 **/
short esp_nr_resp(int origin)
{
   short nret = 0;
   // debug("\n\r\t origin: %d\r\n", origin);
   while (!esp.readable()) {
   }
   while (esp.readable()) {
      char rd;
      esp.read(&rd, 1);
      // debug("%c", rd);
      if (isalnum(rd)) {
         ++nret;
      }
      ThisThread::sleep_for(2ms);
   }
   return nret;
}

/**
 * Local function, set echo on ESP
 * @param on desired echo state(bool)
 **/
void esp_echo(bool on)
{
   if (on) {
      fprintf(espFile, "ATE1\r\n");
      short nret = esp_nr_resp(1);
      if (nret == 2) {
         echo = true;
         // debug("\n\r\tEcho enabled\r\n");
      }
   } else {
      fprintf(espFile, "ATE0\r\n");
      short nret = esp_nr_resp(2);
      if (nret == 2) {
         echo = false;
         // debug("\n\r\tEcho disabled\r\n");
      }
   }
}

bool connect_wifi(void)
{
   bool ok = false;
   if (echo) {
      esp_echo(false);
   }
   fprintf(espFile, "AT+CWJAP=\"%s\",\"%s\"\r\n", shared.ssid, shared.password);
   short nret = esp_nr_resp(3);
   if (nret == 2) {
      cnnctd = true;
      ok = true;
      // debug("\n\r\tWiFi connected\r\n");
   }
   return ok;
}

/**
 * @brief Local function, check WiFi connection
 **/
bool check_wifi(void)
{
   //  debug("cheching");
   bool state = false;
   short nret = 0;
   char ret[25];
   fprintf(espFile, "AT+CIPSTATUS\r\n");
   while (!esp.readable()) {
   }
   while (esp.readable()) {
      char rd;
      esp.read(&rd, 1);
      // debug("%c", rd);
      ret[nret] = rd;
      ++nret;
      ThisThread::sleep_for(2ms);
   }
   if (ret[7] == '2' || ret[7] == '5' || ret[7] == '4') {
      // debug("\n\r\tConnected\r\n");
      state = true;
   }
   //  debug("checking done");
   return state;
}

/**
 * @brief Local function, open TCP link
 **/
void open_link(void)
{
   fprintf(espFile, "AT+CIPMUX=0\r\n");
   esp_nr_resp(4);
   ThisThread::sleep_for(10ms);
   fprintf(espFile, "AT+CIPSTART=\"TCP\",\"%s\",80\r\n", url);
   short nret = esp_nr_resp(5);
   // debug("\t%d", nret);
   ThisThread::sleep_for(10ms);
}

/**
 * Local function, send message to ESP
 * @param msg message (char array)
 **/
bool send(char msg[])
{
   fprintf(espFile, "AT+CIPSEND=%d\r\n", strlen(msg));
   fprintf(espFile, "%s", msg);
   // debug("sent: %s, sz: %d\n\r", msg, strlen(msg));
   // debug("\n\r\t origin: 6\n\r");
   while (!esp.readable()) {
   }
   while (esp.readable()) {
      char rd;
      esp.read(&rd, 1);
      // debug("%c", rd);
      ThisThread::sleep_for(2ms);
   }
   ThisThread::sleep_for(200ms);
   return true;
}

void post_request(time_t bedTime, time_t wakeTime, bool start)
{
   if (!check_wifi()) {
      // debug("\r\n\n\n\n\tconnnect wifi\r\n\n\n\n");
      connect_wifi();
   }
   // debug("\r\n\tposting %d. . .\r\n", (int)start);
   open_link();
   char body[50];
   sprintf(body, "{\"bedtime\":%d,\"waketime\":%d,\"start\":%d}\r\n", (int)bedTime, (int)wakeTime, (int)start);
   char postBuf[300];
   sprintf(postBuf, "POST %s HTTP/1.1\r\nHost: %s\r\nContent-Type: application/json\r\nContent-Length: %d\r\n\r\n%s",
           postAddr, url, sizeof(body) + 3, body);

   if (send(postBuf) && esp.readable()){
      esp_nr_resp(7);
   }
}

// void delete_request(time_t bedTime, time_t wakeTime)
// {
//    debug("\n\t--STRT--\n\r");
//    if (!check_wifi()) {
//       debug("\r\n\n\n\n\tconnnect wifi\r\n\n\n\n");
//       connect_wifi();
//    } else {
//        debug("\n\t--NOWIFNEED--\n\r");
//    }
//    debug("\n\t--HERE--\n\r");
//    open_link();
//    char param[50];
//    sprintf(param, "/?bedtime=%d&waketime:%d", (int)bedTime, (int)wakeTime);
//    char postBuf[250];
//    sprintf(postBuf, "DELETE http://%s%s%s\r\n", url, postAddr, param, url);
//    send(postBuf);
//    esp_nr_resp(8);
// }

/**
 * @brief Local function, recieves and parses return data (for GET)
 **/
int recieve_parse(void)
{
   int timestamp = 0;
   bool jsnStart = false;
   while (!esp.readable()) {
   }
   while (esp.readable()) {
      if (jsnStart) {
         // debug("\tJSON");
         fscanf(espFile, "\"time\": %d", &timestamp);
         jsnStart = false;
      } else {
         char rd;
         esp.read(&rd, 1);
         // debug("%c", rd);
         if (rd == '{') {
            jsnStart = true;
         }
      }
      ThisThread::sleep_for(2ms);
   }
   return timestamp;
}

/**
 * @brief Local function, prepare GET request
 **/
int get(void)
{
   int ret = 100;
   int bckup = (int) time(NULL);
   if (!check_wifi()) {
      connect_wifi();
   }
   open_link();
   char getBuf[200];
   sprintf(getBuf, "GET %s%s%s\n\r", protocol, url, getAddr);
   // debug("\tsending. . .");
   bool sent = send(getBuf);
   // debug("\tcalling parse . . .");
   if (sent) {
      ret = recieve_parse();
   }
   return (ret != 0) ? ret : bckup;
}

bool reset_module(void) {
   fprintf(espFile, "AT+RST\r\n");
   ThisThread::sleep_for(2s);
   esp_nr_resp(8);
   return  connect_wifi();
}

int sync_time(void) { return get(); }
