#include "Screens.h"

I2C i2cBus_disp(D14, D15);
Adafruit_SSD1306_I2c display(i2cBus_disp, NC, 0x78, 64U, 131U);

short head = 0;
uint8_t grph[100] = {0};

/**
 * @brief Local function, prints current time clock on display
 **/
void prnt_clock(void)
{
   char buf[6];
   time_t wake = time(NULL);
   int sz = strftime(buf, 6, "%H:%M", localtime(&wake));
   for (int i = 0; i < sz; ++i) {
      display.drawChar((23 + i * 18), ((64 - 22) / 2), buf[i], 1, 0, 3);
   }
}

/**
 * @brief Local function, activates alarm, if wifi connected uploads to server
 **/
void activate_alarm(time_t wake, time_t actual)
{
   shared.wakeTime = wake;
   shared.bedTime = actual;
   shared.activeScrn = HOME;
   shared.alrmStatus = true;
   if (shared.wifiOk) {
      post_request(actual, shared.wakeTime, true);
   }
}

void Element::show(void)
{
   if (selected) {
      display.drawRect(x, y, w, h, 1);
   } else {
      display.drawRect(x, y, w, h, 0);
   }

   for (unsigned int i = 0; i < text.length(); ++i) {
      display.drawChar((x + 2 + i * (6 * sz)), (y + 2), text[i], 1, 0, sz);
   }
}

void Element::cnstrct(short r, short c, string txt, short nxt, short size)
{
   set = true;
   sz = size;
   text = txt;
   next = nxt;
   w = (text.length() * (6 * sz)) + 4;
   h = (8 * sz) + 3;

   switch (r) {
   case 1: {
      y = 0;
      break;
   }
   case 2: {
      y = 0 + h;
      break;
   }
   case 3: {
      y = 0 + (2 * h);
      break;
   }
   case 4: {
      y = 0 + (3 * h);
      break;
   }
   case 5: {
      y = 64 - (2 * h);
      break;
   }
   case 6: {
      y = 64 - h;
      break;
   }
   }

   switch (c) {
   case 1: {
      x = 2;
      break;
   }
   case 2: {
      x = (130 - w) / 2;
      break;
   }
   case 3: {
      x = 130 - w;
      break;
   }
   }
}

void Element::action(void)
{
   if ((next > -1) && (next < 10)) {
      shared.activeScrn = next;
   } else if ((next > 10) && (next < 20)) {
      time_t actual = time(NULL);
      time_t wake = time(NULL) + 2 * 5400;
      switch (next) {
      case AL_1: {
         activate_alarm(wake, actual);
         break;
      }
      case AL_2: {
         activate_alarm(wake + 5400, actual);
         break;
      }
      case AL_3: {
         activate_alarm(wake + 2 * 5400, actual);
         break;
      }
      case AL_4: {
         activate_alarm(wake + 3 * 5400, actual);
         break;
      }
      case AL_5: {
         activate_alarm(wake + 4 * 5400, actual);
         break;
      }
      case AL_6: {
         activate_alarm(wake + 5 * 5400, actual);
         break;
      }
      }
   } else {
      switch (next) {
      case CNCL: {
         if (shared.alrmStatus) {
            shared.alrmStatus = false;
            if (shared.wifiOk) {
               post_request(shared.bedTime, shared.wakeTime, false);
            }
            shared.wakeTime = 0;
            shared.bedTime = 0;
         }
         shared.activeScrn = HOME;
         break;
      }
      case SYNC: {
         if (shared.wifiOk) {
            int act = sync_time();
            set_time(act);
         }
         shared.activeScrn = HOME;
         break;
      }
      }
   }
}

Screen::Screen(int tp, int prv) : prev(prv), type(tp)
{
   switch (type) {
   case HOME: {
      ptr = &Screen::home;
      elms[2].cnstrct(1, 3, "Budik", PICK, 1);
      elms[3].cnstrct(6, 3, "Nastaveni", STTNGS, 1);
      elms[1].cnstrct(6, 1, "Graf", GRAPH, 1);
      rws = 2;
      clls = 2;
      nBtns = 4;
      r = 1;
      c = 2;
      slctdIndx = 2;
      elms[2].selected = true;
      elms[2].show();
      break;
   }
   case STTNGS: {
      ptr = &Screen::sttngs;
      elms[0].cnstrct(1, 2, "Nastaveni casu", SET_TIME, 1);
      elms[1].cnstrct(2, 2, "Synchronizace casu", SYNC, 1);
      elms[2].cnstrct(3, 2, "Vypnout budik", CNCL, 1);
      rws = 3;
      clls = 1;
      nBtns = 3;
      r = 1;
      c = 1;
      slctdIndx = 0;
      elms[0].selected = true;
      elms[0].show();
      break;
   }
   case GRAPH: {
      ptr = &Screen::graph;
      break;
   }
   case PICK: {
      ptr = &Screen::pick;
      time_t wake = time(NULL) + (2 * 5400);
      char buf1[6];
      strftime(buf1, 6, "%H:%M", localtime(&wake));
      elms[0].cnstrct(1, 2, buf1, AL_1, 1);
      wake += 5400;
      char buf2[6];
      strftime(buf2, 6, "%H:%M", localtime(&wake));
      elms[1].cnstrct(2, 2, buf2, AL_2, 1);
      wake += 5400;
      char buf3[6];
      strftime(buf3, 6, "%H:%M", localtime(&wake));
      elms[2].cnstrct(3, 2, buf3, AL_3, 1);
      wake += 5400;
      char buf4[6];
      strftime(buf4, 6, "%H:%M", localtime(&wake));
      elms[3].cnstrct(4, 2, buf4, AL_4, 1);
      wake += 5400;
      char buf5[6];
      strftime(buf5, 6, "%H:%M", localtime(&wake));
      elms[4].cnstrct(5, 2, buf5, AL_5, 1);
      wake += 5400;
      char buf6[6];
      strftime(buf6, 6, "%H:%M", localtime(&wake));
      elms[5].cnstrct(6, 2, buf6, AL_6, 1);
      rws = 6;
      clls = 1;
      nBtns = 6;
      r = 1;
      c = 1;
      slctdIndx = 0;
      elms[0].selected = true;
      elms[0].show();
      break;
   }
   case SET_TIME: {
      ptr = &Screen::set_time;
      nBtns = 0;
      break;
   }
   case RING: {
      ptr = &Screen::ring;
      nBtns = 0;
      break;
   }
   default: {
      printf("ERR: Uknwn scrn");
      break;
   }
   }
}

void Screen::show(void) { (this->*ptr)(); }

void time_skip(short dir)
{
   time_t cur = time(NULL);
   switch (dir) {
   case 4: {
      cur += 60;
      break;
   }
   case 6: {
      cur -= 60;
      break;
   }
   case 8: {
      cur += 3600;
      break;
   }
   case 2: {
      cur -= 3600;
      break;
   }
   }
   set_time(cur);
}

void Screen::move(short dir)
{
   if (shared.activeScrn != SET_TIME) {
      short origI = slctdIndx;
      short origR = r;
      short origC = c;
      switch (dir) {
      case 4: {
         if (this->c > 1) {
            // printf("lft %d %d\n", c, nBtns);
            --this->c;
            this->slctdIndx -= this->clls;
         }
         break;
      }
      case 6: {
         if (this->c < this->clls) {
            // printf("rght %d %d\n", c, nBtns);
            ++this->c;
            this->slctdIndx += this->clls;
         }
         break;
      }
      case 8: {
         if (this->r > 1) {
            // printf("up %d %d\n", r, nBtns);
            --this->r;
            --this->slctdIndx;
         }
         break;
      }
      case 2: {
         if (this->r < this->rws) {
            // printf("dwn %d %d\n", r, nBtns);
            ++this->r;
            ++this->slctdIndx;
         }
         break;
      }
      }
      if (elms[slctdIndx].set) {
         elms[origI].selected = false;
         elms[origI].show();
         elms[slctdIndx].selected = true;
         elms[slctdIndx].show();
         display.display();
      } else {
         slctdIndx = origI;
         r = origR;
         c = origC;
      }
   } else {
      time_skip(dir);
   }
}

void Screen::home(void)
{
   display.clearDisplay();
   if (shared.alrmStatus) {
      char buf[6];
      strftime(buf, 6, "%H:%M", localtime(&shared.wakeTime));
      for (short i = 0; i < 6; ++i) {
         display.drawChar((4 + i * 6), 2, buf[i], 1, 0, 1);
      }
   } else {
      string status = "VYP.";
      for (short i = 0; i < 4; ++i) {
         display.drawChar((4 + i * 6), 2, status[i], 1, 0, 1);
      }
   }
   prnt_clock();

   for (short i = 0; i < nBtns; ++i) {
      elms[i].show();
   }
   display.display();
}

void Screen::sttngs(void)
{
   display.clearDisplay();
   for (short i = 0; i < nBtns; ++i) {
      elms[i].show();
   }
   display.display();
}

void Screen::graph(void)
{
   display.clearDisplay();

   if (shared.newBpm) {
      shared.newBpm = false;
      grph[head] = 30 + (60 - shared.bpm);
      head = ((head + 1) < 100) ? (head + 1) : ((head + 1) - 100);
   }

   display.drawLine(120, 0, 120, 63, 1);
   display.drawLine(119, 32, 121, 32, 1);
   display.drawChar(113, 29, '6', 1, 0, 1);
   display.drawChar(123, 29, '0', 1, 0, 1);

   display.drawLine(119, 22, 121, 22, 1);
   display.drawLine(119, 42, 121, 42, 1);

   display.drawLine(119, 12, 121, 12, 1);
   display.drawChar(113, 9, '8', 1, 0, 1);
   display.drawChar(123, 9, '0', 1, 0, 1);

   display.drawLine(119, 52, 121, 52, 1);
   display.drawChar(113, 49, '4', 1, 0, 1);
   display.drawChar(123, 49, '0', 1, 0, 1);

   for (short i = 0; i < 100; ++i) {
      short indx = ((head + i) < 100) ? (head + i) : ((head + i) - 100);
      short clr = ((grph[indx]) > 0) ? 1 : 0;
      display.drawPixel(i, grph[indx], clr);
   }

   display.display();
}

void Screen::pick(void)
{
   display.clearDisplay();

   for (short i = 0; i < nBtns; ++i) {
      elms[i].show();
   }

   display.display();
}

void Screen::set_time(void)
{
   display.clearDisplay();
   string status = "Cas:";
   for (short i = 0; i < 4; ++i) {
      display.drawChar((4 + i * 6), 2, status[i], 1, 0, 1);
   }

   char buf[6];
   time_t wake = time(NULL);
   int sz = strftime(buf, 6, "%H:%M", localtime(&wake));
   int x1 = (130 - (sz * 12)) / 2;
   for (int i = 0; i < sz; ++i) {
      display.drawChar(x1 + i * 12, 16, buf[i], 1, 0, 2);
   }

   string hlp1 = "+   hod   -";
   string hlp2 = "+   min   -";
   int x2 = (130 - 11 * 6) / 2;
   for (short i = 0; i < 11; ++i) {
      display.drawChar((x2 + i * 6), 40, hlp1[i], 1, 0, 1);
      display.drawChar((x2 + i * 6), 48, hlp2[i], 1, 0, 1);
   }
   display.fillTriangle(44, 44, 48, 46, 48, 42, 1);
   display.fillTriangle(80, 42, 80, 46, 84, 44, 1);
   display.fillTriangle(44, 53, 46, 49, 48, 53, 1);
   display.fillTriangle(80, 49, 84, 49, 82, 53, 1);

   display.display();
}

void Screen::ring(void)
{
   display.clearDisplay();

   string status = "VSTAVAT";
   for (short i = 0; i < 7; ++i) {
      display.drawChar((5 + i * 18), 21, status[i], 1, 0, 3);
   }

   display.display();
}