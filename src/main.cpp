#include "cstdint"
#include "cstdio"
#include "mbed.h"

#include "DFPlayer.h"
#include "Screens.h"

#define DISP_TM_OT 30 // display timeout in seconds
#define DBNC 100ms    // debounce time
#define LNGPRSS 500ms // longpress time
#define DFLT_TIME 1622811412

#define RNGTN_FL 0x01   // ringotne file number
#define RNGTN_TRCK 0x01 // ringtone track number

#define RING_INTRVL 10 // interval for ringing based on heartrate in minutes
#define WAKE_HRTRT 55  // heartrate treshold for delaying/forwarding waking in BPM

#define SMPL_RATE 2    // sample rate in seconds
#define TRESHOLD 0.7   // beat detection treshold (from 0 to 1)
#define TOLERANCE 0.01 // detection tolerance

AnalogIn vrx(A1); // orig A0, changed for phys. orientation
AnalogIn vry(A0); // orig A1, changed for phys. orientation
InterruptIn bttn(A2);
// DigitalOut myLed(LED1);

AnalogIn hrt(A5);
LowPowerTicker calc;
LowPowerTimer tmr;

LowPowerTicker debounce;
LowPowerTicker longpress;

DigitalIn usr(BUTTON1);

void joystick(int *rx, int *ry, time_t *disp);
void check_button(time_t *disp);
void press(void);
void release(void);

void chck_ring(void);

void count_beats(void);

/**
 * @brief Struct for holding calculated data from heartrate monitor 
 */
typedef struct heartrt_data {
   float curRate;
   short prevRate1;
   short prevRate2;
} hrtrt_data;

hrtrt_data heartrates = {60, 60, 60};

bool dbnc = false;
bool lngprss = true;

bool actvt = false;
bool rtrn = false;

Screen Home(HOME, -1), Settings(STTNGS, HOME), Graph(GRAPH, HOME), SetTime(SET_TIME, STTNGS), Ring(RING, HOME);
Screen *slctn[6] = {&Home, &Settings, &Graph, nullptr, &SetTime, &Ring};

data shared = {.activeScrn = HOME,
               .alrmStatus = false,
               .wakeTime = 0,
               .ringing = false,
               {.ssid = "ssid"},
               {.password = "password"},
               .wifiOk = false,
               .bpm = 0,
               .newBpm = false};

int main()
{
   shared.wifiOk = reset_module();
   ThisThread::sleep_for(1s);
   if (!shared.wifiOk) {
      ThisThread::sleep_for(4s);
   }

   Thread beats;
   beats.start(callback(count_beats));

   display.begin();
   display.clearDisplay();
   display.display();

   bttn.mode(PullUp);
   bttn.fall(callback(press));
   bttn.rise(callback(release));

   int rx = 2;
   int ry = 0;

   time_t dispTmOt = time(NULL) + DISP_TM_OT;

   if (shared.wifiOk) {
      set_time(sync_time());
   } else {
      set_time(DFLT_TIME);
      // debug("no wifi, using default");
   }

   while (usr) {

      // react to stick position
      joystick(&rx, &ry, &dispTmOt);

      // check if display should be drawn
      if (time(NULL) < dispTmOt || shared.ringing) {

         // check for button action
         check_button(&dispTmOt);

         // if graph is being displayed, prevent sleeping
         if (shared.activeScrn == GRAPH) {
            dispTmOt = time(NULL) + DISP_TM_OT;
         }

         // check for ring condition
         if (!shared.ringing && shared.alrmStatus) {
            chck_ring();
         }

         // draw active sreen
         slctn[shared.activeScrn]->show();
         display.display();

      } else {
         display.clearDisplay();
         display.display();
         chck_ring();
      }
   }
}

/**
 * @brief Handle joystick position, trigger appropriate function, reset display sleep
 **/
void joystick(int *rx, int *ry, time_t *disp)
{
   float x = vrx.read();
   float y = vry.read();
   bool chng = false;

   if (x < 0.01) {
      slctn[shared.activeScrn]->move(4);
      chng = true;
   } else if (x > 0.99) {
      slctn[shared.activeScrn]->move(6);
      chng = true;
   } else if (y < 0.01) {
      slctn[shared.activeScrn]->move(2); // orig 8, changed for phys. orientation
      chng = true;
   } else if (y > 0.99) {
      slctn[shared.activeScrn]->move(8); // orig 2, changed for phys. orientation
      chng = true;
   }

   if (chng) {
      *disp = time(NULL) + DISP_TM_OT;
   }
}

/**
 * @brief Check if button has been pressed, react accordingly
 **/
void check_button(time_t *disp)
{
   if (actvt) {
      // printf("short\n");
      short loc = slctn[shared.activeScrn]->slctdIndx;
      slctn[shared.activeScrn]->elms[loc].action();

      if (shared.activeScrn == PICK) {
         if (slctn[PICK] != nullptr) {
            free(slctn[PICK]);
         }
         Screen *Pick = new Screen(PICK, HOME);
         slctn[PICK] = Pick;
      }

      slctn[shared.activeScrn]->show();
      actvt = false;
   } else if (rtrn) {
      // printf("long\n");

      // if ringing, stop
      if (shared.ringing) {
         dfp_stop();
         shared.activeScrn = HOME;
         shared.alrmStatus = false;
         shared.ringing = false;
         shared.wakeTime = 0;
      }

      // if not on homesreen, return to previous screen
      if (shared.activeScrn != HOME) {
         shared.activeScrn = (*slctn[shared.activeScrn]).prev;
      }

      // "reset_dbnc" screen sleep timer
      *disp = time(NULL) + DISP_TM_OT;
      rtrn = false;
   }
}

/**
 * @brief Activate ringing
 **/
void start_ringing(void)
{
   dfp_play(RNGTN_FL, RNGTN_TRCK);
   shared.ringing = true;
   shared.activeScrn = RING;
}

/**
 * @brief Check if ringing conditions have been met
 **/
void chck_ring(void)
{
   time_t current = time(NULL);
   int intrvl = RING_INTRVL * 60;
   if (((shared.wakeTime - intrvl) < current) && (current < (shared.wakeTime + intrvl))) {
      // check if last heartrates are over set treshold
      if ((heartrates.curRate > WAKE_HRTRT) && (heartrates.prevRate1 > WAKE_HRTRT) &&
          (heartrates.prevRate2 > WAKE_HRTRT)) {
         start_ringing();

         // if close to end of interval start ringing regardless of heartrate
      } else if ((shared.wakeTime + intrvl - 30) < current) {
         start_ringing();
      }
   }
}

/**
 * @brief Unlock debounce
 **/
void reset_dbnc(void) { dbnc = false; }

/**
 * @brief Called upon button release, identify short/long press
 **/
void release(void)
{
   if (!dbnc) {
      if (lngprss) {
         rtrn = true;
      } else {
         actvt = true;
      }
   }
}

/**
 * @brief Sets longpress condition, triggered after timeout automatically
 **/
void set_long(void) { lngprss = true; }

/**
 * @brief Initiates button press routine
 **/
void press(void)
{
   dbnc = true;
   lngprss = false;
   debounce.attach(callback(reset_dbnc), DBNC);
   longpress.attach(callback(set_long), LNGPRSS);
}

/**
 * @brief Calculates estimated BPM periodically
 **/
void calc_BPM(heartrt_data *data)
{
   // myLed = !myLed;
   if (data->prevRate2 == 0) {
      shared.bpm = data->curRate;
      data->prevRate2 = data->curRate;
      data->prevRate1 = data->curRate;
   } else {
      shared.bpm = ((data->curRate * 0.7) + (data->prevRate1 * 0.2) + (data->prevRate2 * 0.1));
      data->prevRate2 = data->prevRate1;
      data->prevRate1 = shared.bpm;
   }
   shared.newBpm = true;
}

/**
 * @brief Counts time between beats, estimates BPM
 **/
void count_beats(void)
{
   bool high = false;
   calc.attach(callback(calc_BPM, &heartrates), SMPL_RATE);
   tmr.start();
   while (true) {
      float rd = hrt.read();
      if (high && rd < (TRESHOLD - TOLERANCE)) {
         // debug("RESET\n\r");
         high = false;
      } else if (!high && rd > (TRESHOLD + TOLERANCE)) {
         float rd = tmr.read();
         tmr.reset();
         // debug("BEAT %f\n\r", rd);
         high = true;
         heartrates.curRate = (heartrates.curRate * 0.3) + ((60 / rd) * 0.7);
      }
      wait_us(10);
   }
}